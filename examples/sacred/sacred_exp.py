"""Example how to use LFCNN together with Sacred (https://github.com/IDSIA/sacred)
and a MongoDB Observer.
"""
import configparser
import inspect
import json
import tempfile
from pathlib import Path

# Optional: Set multithreading
import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(4)
tf.config.threading.set_intra_op_parallelism_threads(4)

from tensorflow.keras import optimizers
from tensorflow.keras.callbacks import CSVLogger
from sacred import Experiment
from sacred import SETTINGS
from sacred.observers import MongoObserver

from mdbh import get_uri

from lfcnn import losses
from lfcnn.models import disparity
from lfcnn.callbacks import SacredMetricLogger, SacredTimeLogger, SacredEpochLogger, SacredLearningRateLogger
from lfcnn.callbacks import get as lr_scheduler_get

from lfcnn.metrics import get_central_metrics_small, get_disparity_metrics

# Change sacred capture mode to workaround multiprocessing bug
# https://github.com/IDSIA/sacred/issues/705
SETTINGS['CAPTURE_MODE'] = 'sys'

# Load MongoDB configuration (or set by hand)
# Here, use mdbh helper module (https://gitlab.com/MaxSchambach/mdbh)
# and a config file .mongo.conf in the INI format, see for an example:
# https://gitlab.com/MaxSchambach/mdbh/-/blob/master/examples/.mongo.conf

# Set Sacred MongoDB database name
db_name = "sacred"  # Or use different sacred database name
uri = get_uri("<path-to-.mongo.conf>", db_name)

# Init experiment
ex = Experiment('EpinetTest')
ex.observers.append(MongoObserver(url=uri, db_name=db_name))


@ex.config
def config():
    # Model to use
    model = "Epinet"
    model_type = disparity
    save_weights = True

    # Add source code of the specified model
    model_src = inspect.getfile(model_type.get(model).create_model)
    ex.add_source_file(model_src)

    # Optimizer, Loss
    optimizer = "Adam"
    optimizer_kwargs = dict(learning_rate=0.001)


    epochs = 4
    cycle_epoch = int(0.8 * epochs)
    lr_scheduler = "OneCycle"
    lr_scheduler_kwargs = dict(lr_min=1e-3, lr_max=1e-2, lr_final=1e-5,
                               cycle_epoch=cycle_epoch, max_epoch=epochs)

    loss_disp = "PseudoHuber"
    loss_disp_kwargs = dict(delta=0.1)

    # Set up Data
    dataset = "NewRgbDataset"
    base_path = Path("<path-to-dataset>")
    range_data = 2 ** 16 - 1

    train_path = base_path / "train.h5"
    valid_path = base_path / "validation.h5"
    test_path = base_path / "test.h5"

    base_path = str(base_path)
    train_path = str(train_path)
    valid_path = str(valid_path)
    test_path = str(test_path)

    # Set multiply used attributes for training and testing
    augmented_shape = (9, 9, 32, 32, 3)
    generated_shape = [(32, 32, 9*3) for _ in range(4)]
    batch_size = 32
    data_percentage = 0.05
    validation_freq = 1
    workers = 4
    max_queue_size = 32
    use_multiprocessing = True

    shuffle = False
    augment = dict(flip=True,
                   rotate=True,
                   weigh_chs=True,
                   gamma=True,
                   permute_chs=True,
                   scale=True)

    # SLURM variables
    slurm_cpus_per_task = None
    slurm_mkl_threads = None
    slurm_job_id = None

    # Pack in dicts
    setup_kwargs = dict(
        model=model,
        model_type=model_type,
        save_weights=save_weights,
        optimizer=optimizer,
        optimizer_kwargs=optimizer_kwargs,
        lr_scheduler=lr_scheduler,
        lr_scheduler_kwargs=lr_scheduler_kwargs,
        loss_disp=loss_disp,
        loss_disp_kwargs=loss_disp_kwargs,
    )

    # Set up Training Parameters
    train_kwargs = dict(data=train_path,
                        valid_data=valid_path,
                        data_key="lf",
                        label_keys="disp",
                        augmented_shape=augmented_shape,
                        generated_shape=generated_shape,
                        range_data=range_data,
                        range_valid_data=range_data,
                        batch_size=batch_size,
                        epochs=epochs,
                        data_percentage=data_percentage,
                        valid_percentage=data_percentage,
                        validation_freq=validation_freq,
                        workers=workers,                          # optional
                        max_queue_size=max_queue_size,            # optional
                        use_multiprocessing=use_multiprocessing,  # optional
                        verbose=1                                 # optional
                        )

    # Set up Test Parameters
    test_kwargs = dict(data=test_path,
                       data_key="lf",
                       label_keys="disp",
                       augmented_shape=augmented_shape,
                       generated_shape=generated_shape,
                       range_data=range_data,
                       batch_size=batch_size,
                       data_percentage=data_percentage,
                       workers=workers,                          # optional
                       max_queue_size=max_queue_size,            # optional
                       use_multiprocessing=use_multiprocessing,  # optional
                       verbose=1                                 # optional
                       )


@ex.capture
def run_experiment(setup_kwargs, train_kwargs, test_kwargs, _run):

    # Load and init optimizer
    optimizer_dict = dict(class_name=setup_kwargs['optimizer'],
                          config=setup_kwargs['optimizer_kwargs'])
    optimizer = optimizers.get(optimizer_dict)

    # Load and init losses
    loss_disp = losses.get(setup_kwargs['loss_disp'])
    loss_disp = loss_disp(**setup_kwargs['loss_disp_kwargs'])

    loss = dict(disparity=loss_disp)
    metrics = dict(disparity=get_disparity_metrics())

    # Context manager for temporary directory
    with tempfile.TemporaryDirectory() as tmp_dir:

        # Set up callbacks
        csv_log_path = tmp_dir + "/training_validation.csv"
        csv_logger = CSVLogger(csv_log_path, separator=',', append=False)
        sacred_metric_logger = SacredMetricLogger(_run)
        sacred_lr_logger = SacredLearningRateLogger(_run)
        sacred_time_logger = SacredTimeLogger(_run)
        sacred_epoch_logger = SacredEpochLogger(_run, epochs=train_kwargs['epochs'])

        # Learning rate scheduler if set
        if setup_kwargs['lr_scheduler'] is not None:
            lr_scheduler = lr_scheduler_get(setup_kwargs['lr_scheduler'])
            lr_scheduler = lr_scheduler(**setup_kwargs['lr_scheduler_kwargs'])
            callbacks = [lr_scheduler, csv_logger, sacred_metric_logger,
                         sacred_time_logger, sacred_epoch_logger, sacred_lr_logger]
        else:
            callbacks = [csv_logger, sacred_metric_logger, sacred_time_logger,
                         sacred_epoch_logger, sacred_lr_logger]


        # Load and init model
        model_type = setup_kwargs['model_type']
        model = model_type.get(setup_kwargs['model'])
        model = model(optimizer=optimizer, loss=loss, metrics=metrics, callbacks=callbacks)

        # Train and validate
        hist = model.train(**train_kwargs)

        if setup_kwargs['save_weights']:
            weights_path = tmp_dir + "/weights.h5"
            model.save_weights(weights_path)
            ex.add_artifact(weights_path)

        # Test
        test_res = model.test(**test_kwargs)
        print(test_res)

        # Log test results to sacred
        for key in test_res.keys():
            _run.log_scalar("test_" + key, test_res[key], 0)

        # Also, save test results as JSON artifact
        test_path = tmp_dir + "/test.json"
        with open(test_path, 'w') as outfile:
            json.dump(test_res, outfile, indent=4)

        # Save artifacts
        ex.add_artifact(csv_log_path)
        ex.add_artifact(test_path)


@ex.automain
def main():
    run_experiment()
