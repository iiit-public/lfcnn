"""Example how to use LFCNN
together with Sacred (https://github.com/IDSIA/sacred)
and a MongoDB Observer with all data loaded into RAM.
"""
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import inspect
import tempfile
from pathlib import Path
import numpy as np

# Optional: Set multithreading
import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(4)
tf.config.threading.set_intra_op_parallelism_threads(4)

from tensorflow.keras import optimizers

from sacred import Experiment
from sacred import SETTINGS
from sacred.observers import MongoObserver

from mdbh import get_uri

from lfcnn import losses
from lfcnn.models import disparity, superresolution, autoencoder
from lfcnn.callbacks import LearningRateFinder, SacredEpochLogger, SacredTimeLogger, SacredLearningRateLogger
from lfcnn.metrics import get_disparity_metrics, get_lf_metrics, get_central_metrics_fullsize, get_central_metrics_small

# Change sacred capture mode to workaround multiprocessing bug
# https://github.com/IDSIA/sacred/issues/705
SETTINGS['CAPTURE_MODE'] = 'sys'

# Load MongoDB configuration (or set by hand)
# Here, use mdbh helper module (https://gitlab.com/MaxSchambach/mdbh)
# and a config file .mongo.conf in the INI format, see for an example:
# https://gitlab.com/MaxSchambach/mdbh/-/blob/master/examples/.mongo.conf

# Set Sacred MongoDB database name
db_name = "sacred"  # Or use different sacred database name
uri = get_uri("<path-to-.mongo.conf>", db_name)

# Init experiment
ex = Experiment('LrFinder')
ex.observers.append(MongoObserver(url=uri, db_name=db_name))


@ex.config
def config():
    # Model to use
    model = "Epinet"
    model_type = disparity

    model_source = inspect.getfile(model_type.get(model).create_model)
    ex.add_source_file(model_source)

    # Optimizer, Loss
    optimizer = "SGD"
    lr_min = 1e-5
    lr_max = 10
    optimizer_kwargs = dict(learning_rate=lr_min)

    loss = "PseudoHuber"
    loss_kwargs = dict(delta=0.125)

    # Set up Data
    dataset = "NewRgbDataset"
    base_path = Path("<path-to-dataset>")
    range_data = 2 ** 16 - 1  # 16 Bit data
    num_data = 78400
    epochs = 1

    train_path = base_path / "train.h5"
    base_path = str(base_path)
    train_path = str(train_path)

    # Set attributes for training and testing
    augmented_shape = (9, 9, 32, 32, 3)
    generated_shape = [(32, 32, 9 * 3) for _ in range(4)]
    batch_size = 256
    data_percentage = 1
    num_batches = int(epochs * num_data / batch_size)

    gpus = 1
    cpu_merge = False

    shuffle = False
    augment = dict(flip=False,
                   rotate=False,
                   weigh_chs=False,
                   gamma=False,
                   permute_chs=False,
                   scale=False)

    workers = 4
    max_queue_size = 8
    use_multiprocessing = True

    # SLURM variables
    slurm_cpus_per_task = None
    slurm_mkl_threads = None
    slurm_job_id = None

    # Pack in dicts
    setup_kwargs = dict(
        model=model,
        model_type=model_type,
        optimizer=optimizer,
        lr_min=lr_min,
        lr_max=lr_max,
        num_batches=num_batches,
        optimizer_kwargs=optimizer_kwargs,
        loss=loss,
        loss_kwargs=loss_kwargs,
    )

    # Set up Training Parameters
    train_kwargs = dict(data=train_path,
                        valid_data=None,
                        data_key="lf",
                        label_keys="disp",
                        augmented_shape=augmented_shape,
                        generated_shape=generated_shape,
                        range_data=range_data,
                        batch_size=batch_size,
                        epochs=epochs,
                        data_percentage=data_percentage,
                        shuffle=shuffle,
                        augment=augment,
                        gpus=gpus,
                        cpu_merge=cpu_merge,
                        workers=workers,
                        max_queue_size=max_queue_size,
                        use_multiprocessing=use_multiprocessing,
                        verbose=1
                        )

@ex.capture
def run_experiment(setup_kwargs, train_kwargs, _run, _log):

    # Set up lr finder callback
    lr_min, lr_max = setup_kwargs['lr_min'], setup_kwargs['lr_max']
    num_batches = setup_kwargs['num_batches']
    lr_finder = LearningRateFinder(lr_min=lr_min, lr_max=lr_max, num_batches=num_batches, verbose=False)
    sacred_time_logger = SacredTimeLogger(_run)
    sacred_epoch_logger = SacredEpochLogger(_run, epochs=train_kwargs['epochs'])
    callbacks = [lr_finder, sacred_time_logger, sacred_epoch_logger]

    # Load and init model
    model_type = setup_kwargs['model_type']
    model = model_type.get(setup_kwargs['model'])

    # Load and init optimizer
    optimizer_dict = dict(class_name=setup_kwargs['optimizer'],
                          config=setup_kwargs['optimizer_kwargs'])
    optimizer = optimizers.get(optimizer_dict)

    # Load and init losses
    loss = losses.get(setup_kwargs['loss'])
    loss = loss(**setup_kwargs['loss_kwargs'])

    if setup_kwargs['model'].lower() == "epinet":
        loss = dict(disparity=loss)
        metrics = dict(disparity=get_disparity_metrics())

    elif setup_kwargs['model'].lower() == "conv3dmodel":
        loss = dict(light_field=loss)
        metrics = dict(light_field=get_lf_metrics())

    elif setup_kwargs['model'].lower() == "sasconv":
        loss = dict(light_field=loss)
        metrics = dict(light_field=get_lf_metrics())

    else:
        raise ValueError(f"Unknown model {setup_kwargs['model']}")

    model = model(optimizer=optimizer, loss=loss, metrics=metrics, callbacks=callbacks)

    # Train
    hist = model.train(**train_kwargs)

    print("LRs: ", lr_finder.lrs)
    print("Losses: ", lr_finder.losses)

    # Log lr finder results to sacred
    for i, lr in enumerate(lr_finder.lrs):
        _run.log_scalar("lr", lr, i)
    for i, loss in enumerate(lr_finder.losses):
        _run.log_scalar("loss", loss, i)
    for i, avg in enumerate(lr_finder.avg_losses):
        _run.log_scalar("avg_loss", avg, i)

    # Plot and save as PNG, crop n
    with tempfile.NamedTemporaryFile(suffix='.png') as file:
        n = 10
        fig = plt.figure()
        ax = fig.add_subplot(111)
        lr = lr_finder.lrs[n:-n]
        loss = lr_finder.losses[n:-n]
        av = 7
        loss = np.convolve(loss, (1.0/av)*np.ones(av), 'same')
        ax.loglog(lr, loss)
        fig.savefig(file.name)
        ex.add_artifact(file.name)

    return


@ex.automain
def main():
    run_experiment()
