"""LFCNN example"""

# Optionally set CPU as device:
# import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

from pathlib import Path

from lfcnn.models.disparity import Epinet
from lfcnn.losses import PseudoHuber
from lfcnn.metrics import get_disparity_metrics
from lfcnn.callbacks import LearningRateFinder

from tensorflow.keras.optimizers import Nadam


# Set up Data
data_base_path = Path("<path-to-data>")
data_path = data_base_path / "train.h5"
valid_path = data_base_path / "validation.h5"
test_path = data_base_path / "test.h5"

# Set up Loss and Metrics
loss = dict(disparity=PseudoHuber(delta=0.1))
metrics = dict(disparity=get_disparity_metrics())

# Set up lr finder callback
lr_finder = LearningRateFinder(lr_min=1e-5, lr_max=1, num_batches=551, verbose=True)
callbacks = [lr_finder]

# Set up optimizer
optimizer = Nadam(learning_rate=lr_finder.lr_min)

# Set multiply used attributes for training and testing
augmented_shape = (9, 9, 32, 32, 3)
generated_shape = [(32, 32, 9*3) for _ in range(4)]
range_data = 2**16 - 1
batch_size = 128
epochs = 1
data_percentage = 1
workers = 6
max_queue_size = 32
use_multiprocessing = True


# Set up Training Parameters
train_kwargs = dict(data=data_path,
                    valid_data=None,
                    data_key="lf",
                    label_keys="disp",
                    augmented_shape=augmented_shape,
                    generated_shape=generated_shape,
                    range_data=range_data,
                    range_valid_data=range_data,
                    batch_size=batch_size,
                    epochs=epochs,
                    data_percentage=data_percentage,
                    valid_percentage=data_percentage,
                    validation_freq=1,                        # optional
                    workers=workers,                          # optional
                    max_queue_size=max_queue_size,            # optional
                    use_multiprocessing=use_multiprocessing,  # optional
                    verbose=1                                 # optional
                    )
# Create model
model = Epinet(optimizer=optimizer, loss=loss, metrics=metrics, callbacks=callbacks)

# Train and validate
hist = model.train(**train_kwargs)
print(hist.history)

print("LRs: ", lr_finder.lrs)
print("Losses: ", lr_finder.losses)

import matplotlib.pyplot as plt
plt.figure()
plt.plot(lr_finder.lrs, lr_finder.losses)
plt.figure()
