"""LFCNN example"""

from pathlib import Path

from tensorflow.keras.optimizers import RMSprop

from lfcnn.models.disparity import Epinet
from lfcnn.losses import PseudoHuber
from lfcnn.metrics import get_disparity_metrics
from lfcnn.callbacks import LinearDecay


# Set up Data
data_base_path = Path("<path-to-dataset>")
data_range = 2**16 - 1  # 16 bit data
data_path = data_base_path / "train.h5"
valid_path = data_base_path / "validation.h5"
test_path = data_base_path / "test.h5"

# Set up Loss and Metrics
loss = dict(disparity=PseudoHuber(delta=0.1))
metrics = dict(disparity=get_disparity_metrics())

# Set up callbacks
epochs = 16
lr_sched = LinearDecay(lr_init=1e-5, lr_min=1e-6, max_epoch=epochs)
callbacks = [lr_sched]

# Set up optimizer
optimizer = RMSprop(learning_rate=1e-5)

# Set multiply used attributes for training and testing
augmented_shape = (9, 9, 32, 32, 3)
generated_shape = [(32, 32, 9, 1) for _ in range(4)]
augment = dict(flip=True, scale=True, gamma=True)
to_mono = True
batch_size = 16
data_percentage = 0.005
workers = 0
max_queue_size = 16
use_multiprocessing = False


# Set up Training Parameters
train_kwargs = dict(data=data_path,
                    valid_data=valid_path,
                    data_key="lf",
                    label_keys="disp",
                    augmented_shape=augmented_shape,
                    generated_shape=generated_shape,
                    range_data=data_range,
                    range_valid_data=data_range,
                    batch_size=batch_size,
                    epochs=epochs,
                    data_percentage=data_percentage,
                    valid_percentage=data_percentage,
                    augment=augment,
                    gen_kwargs=dict(to_mono=True),
                    workers=workers,                          # optional
                    max_queue_size=max_queue_size,            # optional
                    use_multiprocessing=use_multiprocessing,  # optional
                    verbose=1                                 # optional
                    )

# Set up Test Parameters
test_kwargs = dict(data=test_path,
                   data_key="lf",
                   label_keys="disp",
                   augmented_shape=augmented_shape,
                   generated_shape=generated_shape,
                   range_data=data_range,
                   batch_size=batch_size,
                   data_percentage=data_percentage,
                   workers=workers,                          # optional
                   max_queue_size=max_queue_size,            # optional
                   use_multiprocessing=use_multiprocessing,  # optional
                   verbose=1                                 # optional
                   )

# Create model
model = Epinet(optimizer=optimizer, loss=loss, metrics=metrics, callbacks=callbacks)

# Train and validate
hist = model.train(**train_kwargs)
print(hist.history)

# Test
test_res = model.test(**test_kwargs)
print(test_res)
