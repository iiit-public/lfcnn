"""LFCNN example"""

# Optionally set CPU as device:
# import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

from pathlib import Path

from lfcnn.models.center_and_disparity import Conv3dDecode2d
from lfcnn.losses import Huber
from lfcnn.metrics import get_disparity_metrics, get_central_metrics_small

from tensorflow.keras.optimizers import Nadam


# Set up Data
data_base_path = Path("<path-to-dataset>")
data_range = 2**16 - 1  # 16 bit data
data_path = data_base_path / "train.h5"
valid_path = data_base_path / "validation.h5"
test_path = data_base_path / "test.h5"

# Set up Loss and Metrics
loss = dict(disparity=Huber(delta=1.0), central_view=Huber(delta=1.0))
metrics = dict(disparity=get_disparity_metrics(), central_view=get_central_metrics_small())

# Set up callbacks
callbacks = []

# Set up optimizer
lrate = 0.002
optimizer = Nadam(learning_rate=lrate)

# Set multiply used attributes for training and testing
augmented_shape = (9, 9, 32, 32, 3)
generated_shape = (32, 32, 9*9, 3)
batch_size = 4
epochs = 16
data_percentage = 0.005
workers = 0
max_queue_size = 16
use_multiprocessing = False


# Set up Training Parameters
train_kwargs = dict(data=data_path,
                    valid_data=valid_path,
                    data_key="lf",
                    label_keys="disp",
                    augmented_shape=augmented_shape,
                    generated_shape=generated_shape,
                    range_data=data_range,
                    range_valid_data=data_range,
                    batch_size=batch_size,
                    epochs=epochs,
                    data_percentage=data_percentage,
                    valid_percentage=data_percentage,
                    workers=workers,                          # optional
                    max_queue_size=max_queue_size,            # optional
                    use_multiprocessing=use_multiprocessing,  # optional
                    verbose=1                                 # optional
                    )

# Set up Test Parameters
test_kwargs = dict(data=test_path,
                   data_key="lf",
                   label_keys="disp",
                   augmented_shape=augmented_shape,
                   generated_shape=generated_shape,
                   range_data=data_range,
                   batch_size=batch_size,
                   data_percentage=data_percentage,
                   workers=workers,                          # optional
                   max_queue_size=max_queue_size,            # optional
                   use_multiprocessing=use_multiprocessing,  # optional
                   verbose=1                                 # optional
                   )

# Create model
model = Conv3dDecode2d(optimizer=optimizer, loss=loss,
                       metrics=metrics, callbacks=callbacks)

# Train and validate
hist = model.train(**train_kwargs)
print(hist.history)

# Test
test_res = model.test(**test_kwargs)
print(test_res)
