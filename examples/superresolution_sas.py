"""LFCNN example"""

# Optionally set CPU as device:
# import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

from pathlib import Path

from lfcnn.models.superresolution import SasConv
from lfcnn.losses import PseudoHuber
from lfcnn.metrics import get_lf_metrics
from lfcnn.callbacks import OneCycle, OneCycleMomentum

from tensorflow.keras.optimizers import SGD, Adam


# Set up Data
data_base_path = Path("<path-to-dataset>")
data_range = 2**16 - 1  # 16 bit data
data_path = data_base_path / "train.h5"
valid_path = data_base_path / "validation.h5"
test_path = data_base_path / "test.h5"

# Set up Loss and Metrics
loss = dict(light_field=PseudoHuber(delta=0.1))
metrics = dict(light_field=get_lf_metrics())

# Set up callbacks
epochs = 16
# cycle_epoch = int(0.8*epochs)
# lr_min = 1e-3
# lr_max = 1-2
# lr_final = 1e-5
# lr_sched = OneCycle(lr_min=lr_min, lr_max=lr_max, lr_final=lr_final,
#                     cycle_epoch=cycle_epoch, max_epoch=epochs)
# m_sched = OneCycleMomentum(cycle_epoch=cycle_epoch)
# callbacks = [lr_sched, m_sched]

# Set up optimizer
# optimizer = SGD(learning_rate=1e-3)
optimizer = Adam()
callbacks = []

# Set multiply used attributes for training and testing
augmented_shape = (9, 9, 32, 32, 3)
generated_shape = (9*9, 16, 16, 1)
batch_size = 16
data_percentage = 0.005
workers = 0
max_queue_size = 16
use_multiprocessing = False
gen_kwargs = dict(du=1, dv=1, ds=2, dt=2, bw=True)


# Set up Training Parameters
train_kwargs = dict(data=data_path,
                    valid_data=valid_path,
                    data_key="lf",
                    label_keys=None,
                    augmented_shape=augmented_shape,
                    generated_shape=generated_shape,
                    range_data=data_range,
                    range_valid_data=data_range,
                    batch_size=batch_size,
                    epochs=epochs,
                    data_percentage=data_percentage,
                    valid_percentage=data_percentage,
                    validation_freq=1,                        # optional
                    workers=workers,                          # optional
                    max_queue_size=max_queue_size,            # optional
                    use_multiprocessing=use_multiprocessing,  # optional
                    verbose=1,                                # optional
                    gen_kwargs=gen_kwargs
                    )

# Set up Test Parameters
test_kwargs = dict(data=test_path,
                   data_key="lf",
                   label_keys=None,
                   augmented_shape=augmented_shape,
                   generated_shape=generated_shape,
                   range_data=data_range,
                   batch_size=batch_size,
                   data_percentage=data_percentage,
                   workers=workers,                          # optional
                   max_queue_size=max_queue_size,            # optional
                   use_multiprocessing=use_multiprocessing,  # optional
                   verbose=1,                                # optional
                   gen_kwargs=gen_kwargs
                   )

# Create model
model = SasConv(optimizer=optimizer, loss=loss, metrics=metrics, callbacks=callbacks)

# Train and validate
hist = model.train(**train_kwargs)
print(hist.history)

# Test
test_res = model.test(**test_kwargs)
print(test_res)
