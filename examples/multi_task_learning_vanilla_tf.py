"""Example showing the use of the multi-task training strategy using a pure
TensorFlow approach (not utilizing the LFCNN model class)."""

# Optionally set CPU as device:
# from lfcnn.utils.tf_utils import use_cpu
# use_cpu()

from lfcnn.losses import Huber
from lfcnn.losses import SSIM, CosineProximity
from lfcnn.training.multi_task import MultiTaskUncertainty

from tensorflow import keras
from tensorflow.keras.optimizers import Nadam

# Create a dummy model using MNIST images as input.
# This dummy model will be multi-task: autoencoder and classifier
inputs = keras.Input(shape=(32, 32, 3))

conv_args = dict(kernel_size=3, activation="relu", padding="same")

# Encoder
x = keras.layers.Conv2D(16, **conv_args)(inputs)
x = keras.layers.Conv2D(32,strides=2, **conv_args)(x)  # Downsample
x = keras.layers.Conv2D(32, **conv_args)(x)
x = keras.layers.Conv2D(64, strides=2, **conv_args)(x)  # Downsample

# Latent space
x = keras.layers.Conv2D(128, **conv_args)(x)

# Decoder A (autoencoder path)
x_a = keras.layers.Conv2DTranspose(64, strides=2, **conv_args)(x)  # Upsample
x_a = keras.layers.Conv2D(32, **conv_args)(x_a)
x_a = keras.layers.Conv2DTranspose(32, strides=2, **conv_args)(x_a)  # Upsample
x_a = keras.layers.Conv2D(16, **conv_args)(x_a)
reconstruction = keras.layers.Conv2D(3, name="reconstruction", **conv_args)(x_a)

# Decoder B (classifier)
x_b = keras.layers.Conv2DTranspose(64, strides=2, **conv_args)(x)  # Upsample
x_b = keras.layers.Conv2D(32, **conv_args)(x_b)
x_b = keras.layers.Conv2DTranspose(32, strides=2, **conv_args)(x_b)  # Upsample
x_b = keras.layers.Conv2D(16, **conv_args)(x_b)
x_b = keras.layers.Conv2D(3, **conv_args)(x_b)
x_b = keras.layers.Reshape((32*32*3,))(x_b)
x_b = keras.layers.Dense(64, activation="relu")(x_b)
x_b = keras.layers.Dense(64, activation="relu")(x_b)
labels = keras.layers.Dense(10, name="classifier")(x_b)

# Instead of keras.Model use the provided subclass
# model = keras.Model(inputs=inputs, outputs=outputs, name="mnist_model")
model = MultiTaskUncertainty(inputs=inputs, outputs=[reconstruction, labels], name="dummy_model")

# Use MNIST dataset, labels not needed
(x_train, label_train), (x_test, label_test) = keras.datasets.cifar10.load_data()

x_train = x_train.astype("float32") / 255.0
x_test = x_test.astype("float32") / 255.0

model.compile(
    loss=dict(reconstruction=Huber(delta=1.0),
              classifier=keras.losses.SparseCategoricalCrossentropy(from_logits=True)),
    optimizer=Nadam(),
    metrics=dict(reconstruction=["MSE", SSIM(), CosineProximity()],
                 classifier=["accuracy"])
)

# It is important to wrap the labels in a dictionary with the corresponding
# output layer names!
history = model.fit(x_train,
                    dict(reconstruction=x_train, classifier=label_train),
                    batch_size=64,
                    epochs=2,
                    validation_split=0.2)

test_scores = model.evaluate(x_test,
                             dict(reconstruction=x_test, classifier=label_test),
                             verbose=2)
test_res = {key: val for key, val in zip(model.metrics_names, test_scores)}
print("Test:", test_res)
