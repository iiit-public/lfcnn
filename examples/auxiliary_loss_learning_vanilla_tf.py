"""Example showing the use of the auxiliary loss training strategy using a pure
TensorFlow approach (not utilizing the LFCNN model class)."""

# Optionally set CPU as device:
# from lfcnn.utils.tf_utils import use_cpu
# use_cpu()

from pathlib import Path

from lfcnn.losses import Huber
from lfcnn.losses import SSIM, CosineProximity
from lfcnn.losses import NormalizedStructuralSimilarity, NormalizedCosineProximity
from lfcnn.training.auxiliary_loss import NormalizedGradientSimilarity

from tensorflow import keras
from tensorflow.keras.optimizers import Nadam

# Create a dummy autoencoder model using MNIST images as input
inputs = keras.Input(shape=(32, 32, 3))

conv_args = dict(kernel_size=3, activation="relu", padding="same")

# Encoder
x = keras.layers.Conv2D(16, **conv_args)(inputs)
x = keras.layers.Conv2D(32,strides=2, **conv_args)(x)  # Downsample
x = keras.layers.Conv2D(32, **conv_args)(x)
x = keras.layers.Conv2D(64, strides=2, **conv_args)(x)  # Downsample

# Latent space
x = keras.layers.Conv2D(128, **conv_args)(x)

# Decoder
x = keras.layers.Conv2DTranspose(64, strides=2, **conv_args)(x)  # Upsample
x = keras.layers.Conv2D(32, **conv_args)(x)
x = keras.layers.Conv2DTranspose(32, strides=2, **conv_args)(x)  # Upsample
x = keras.layers.Conv2D(16, **conv_args)(x)
outputs = keras.layers.Conv2D(3, name="reconstruction", **conv_args)(x)

# Instead of keras.Model use the provided subclass, aux_losses are not instantiated!
# model = keras.Model(inputs=inputs, outputs=outputs, name="mnist_model")

model = NormalizedGradientSimilarity(inputs=inputs, outputs=outputs, name="dummy_model",
                                     aux_losses=dict(reconstruction=[NormalizedStructuralSimilarity, NormalizedCosineProximity]))

# Use MNIST dataset, labels not needed
(x_train, _), (x_test, _) = keras.datasets.cifar10.load_data()

x_train = x_train.astype("float32") / 255.0
x_test = x_test.astype("float32") / 255.0

model.compile(
    loss=Huber(delta=1.0),
    optimizer=Nadam(),
    metrics=["MSE", SSIM(), CosineProximity()],
)

# Note that, for also for single-task models it is important to wrapt the
# labels in a dictionary with the corresponding name.
# Otherwise, NormGradSim may give an error.
# The name is identical to the name of the last (output) layer.
history = model.fit(x_train, dict(reconstruction=x_train),
                    batch_size=64,
                    epochs=2,
                    validation_split=0.2)

test_scores = model.evaluate(x_test, x_test, verbose=2)
print("Test MSE:", test_scores[1])
print("Test SSIM:", test_scores[2])
print("Test Cosine Proximity:", test_scores[3])