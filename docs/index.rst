.. lfcnn documentation master file

Welcome to lfcnn's documentation!
==================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   Introduction <README.rst>
   User Documentation <user-doc.rst>
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

