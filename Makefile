init:
	pip install -r requirements.txt .

editable:
	pip install -r requirements.txt -e .

all: init
	pip install -r requirements.txt .[utils]

test: init
	pytest test

coverage: init
	pytest --cov=lfcnn --cov-report term-missing

uninstall:
	pip uninstall lfcnn
